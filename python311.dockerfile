FROM python:3.11

RUN apt-get update && apt-get install -y \
    binutils \
    libproj-dev \
    gdal-bin \
    python3-psycopg2 \
    pipenv \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir wheel
